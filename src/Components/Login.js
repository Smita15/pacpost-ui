import React, { Component } from "react";
import { Button, Form, Message , Checkbox } from "semantic-ui-react";

class Login extends Component {
   
    render() {
      return (
      <div>
        <div className="left-content">
            <div className="login_image"></div>
        </div> 

        <div className="right-content">
          <div className="Container">
            <div className="heading">
              <p className="heading-text">Sign in to your account</p>
              <hr className="top"/>
            </div>
            <div className="form-content">
              <Form>
                <Form.Input
                      error={{ content: 'Forgot E-mail?'}}
                      fluid
                      label='E-mail' type="text"
                      className="auth-input-field"
                      />

                <Form.Input
                      error={{ content: 'Forgot Passwor?' }}
                      fluid
                      label='Password' type="password"
                      className="auth-input-field"
                      />

                <Form.Checkbox
                      fluid
                      label='First name' label='Remember me' />

                <Button color='purple' >Sign In</Button>
              </Form>
            </div>
            <hr className="bottom"/>
            <div className="form-content">
                <p>By creating an account or continuing to use a Pacpost Live you acknowledge and agree that <br/>
                you have acceped the <b>Terms of Service</b> and reviewed the <b>Privacy Policy</b></p>
            </div>
          </div>
        </div>
      </div>
      );
    }
  }
  export default Login;
