import React, { Component } from "react";
import "./App.css";
import "semantic-ui-css/semantic.min.css";
import { BrowserRouter, Route, /*Link,*/ Switch } from "react-router-dom";
import Login from "./Components/Login";
import Terms from "./Components/Terms";

export default class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="container">
          <div className="header">
            <div className="logo">  
            </div>
          </div>
        <div>
          
            <Switch>
              <Route exact path="/Login" component={Login} />
              
              <Route exact path="/Terms" component={Terms} />
              

            </Switch>
          </div>
        </div>
      </BrowserRouter>
    );
  }
}
